import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  Client,
  ClientPaginationResponse,
  TablepocApiService,
} from '@animana-table-poc/table-poc/data';
import { ComponentStore } from '@ngrx/component-store';
import { finalize, map, take, tap } from 'rxjs/operators';

@Component({
  selector: 'animana-table-poc-plain-table',
  templateUrl: './plain-table.component.html',
  styleUrls: ['./plain-table.component.scss'],
  providers: [ComponentStore],
  encapsulation: ViewEncapsulation.None,
})
export class PlainTableComponent implements OnInit {
  public selectedClients: Client[] = [];

  public loading = false;

  public searchValue = '';

  readonly clients$ = this.componentStore.state$.pipe(
    map((state) => state.content)
  );

  constructor(
    private readonly api: TablepocApiService,
    private readonly componentStore: ComponentStore<ClientPaginationResponse>
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.api
      .getClients()
      .pipe(
        take(1),
        tap((response) => this.componentStore.setState({ ...response })),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }
}
