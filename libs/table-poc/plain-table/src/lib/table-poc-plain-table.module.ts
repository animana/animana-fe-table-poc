import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { PlainTableComponent } from './views/plain-table/plain-table.component';

export const tablePocPlainTableRoutes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: PlainTableComponent,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(tablePocPlainTableRoutes)],
  exports: [RouterModule],
  declarations: [PlainTableComponent],
})
export class TablePocPlainTableModule {}
