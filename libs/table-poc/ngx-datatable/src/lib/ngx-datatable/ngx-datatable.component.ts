import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Client, ClientPaginationResponse, TablepocApiService } from '@animana-table-poc/table-poc/data';
import { ComponentStore } from '@ngrx/component-store';
import { finalize, map, take, tap } from 'rxjs/operators';
import { ColumnMode, SelectionType, SortType } from '@swimlane/ngx-datatable';

@Component({
  selector: 'animana-table-poc-ngx-datatable',
  templateUrl: './ngx-datatable.component.html',
  styleUrls: ['./ngx-datatable.component.scss'],
  providers: [ComponentStore],
  encapsulation: ViewEncapsulation.None
})
export class NgxDatatableComponent implements OnInit {
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  SortType = SortType;

  public selectedClients: Client[] = [];

  public loading = false;

  public searchValue = '';

  readonly clients$ = this.componentStore.state$.pipe(
    map((state) => state.content)
  );

  constructor(
    private readonly api: TablepocApiService,
    private readonly componentStore: ComponentStore<ClientPaginationResponse>
  ) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.api
      .getClients()
      .pipe(
        take(1),
        tap((response) => this.componentStore.setState({ ...response })),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selectedClients);

    this.selectedClients.splice(0, this.selectedClients.length);
    this.selectedClients.push(...selected);
  }

  onActivate(event) {
    console.log('Activate Event', event);
  }
}
