import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { NgxDatatableComponent } from './ngx-datatable/ngx-datatable.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

export const tablePocNgxDatatableRoutes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: NgxDatatableComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(tablePocNgxDatatableRoutes),
    NgxDatatableModule,
  ],
  exports: [RouterModule],
  declarations: [NgxDatatableComponent],
})
export class TablePocNgxDatatableModule {}
