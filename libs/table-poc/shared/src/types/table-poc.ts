import { Client } from '@animana-table-poc/table-poc/data';

export interface TablePocStore {
  clients: Client[];
  pagination: {
    page: number;
    size: number;
  };
}
