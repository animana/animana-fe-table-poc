export { TablePocPrimeNgCoreModule } from './lib/table-poc-prime-ng-core.module';

export { ClientsQuery } from './lib/queries/clients.query';
export { ClientService } from './lib/services/client.service';

export * from './types/table-poc';
