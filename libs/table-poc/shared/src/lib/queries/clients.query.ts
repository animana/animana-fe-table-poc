import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ClientsState, ClientsStore } from '../stores/clients.store';

@Injectable({ providedIn: 'root' })
export class ClientsQuery extends QueryEntity<ClientsState> {
  /**
   * Display active clients only
   */
  public activeClients$ = this.selectAll({
    filterBy: (client) => client.active,
  });

  constructor(protected store: ClientsStore) {
    super(store);
  }
}
