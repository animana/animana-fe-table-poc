import { Injectable } from '@angular/core';
import { Client } from '@animana-table-poc/table-poc/data';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export interface ClientsState extends EntityState<Client> {
  ui: {
    page: number;
    size: number;
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'Clients' })
export class ClientsStore extends EntityStore<ClientsState> {
  constructor() {
    super();
  }
}
