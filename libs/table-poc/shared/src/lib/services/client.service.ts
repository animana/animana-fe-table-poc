import { Injectable } from '@angular/core';
import { Client, TablepocApiService } from '@animana-table-poc/table-poc/data';
import { map, take, tap } from 'rxjs/operators';
import { ClientsStore } from '../stores/clients.store';
import { ClientsQuery } from '../queries/clients.query';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  constructor(
    private readonly api: TablepocApiService,
    private readonly store: ClientsStore,
    public readonly query: ClientsQuery
  ) {}

  /**
   * Fetch the clients and pipe them to the store
   */
  getClients(): Observable<Client[]> {
    return this.api.getClients().pipe(
      take(1),
      tap(({ content, page, size }) => {
        this.store.add(content);
        this.store.update((state) => ({
          ...state,
          ui: {
            page,
            size,
          },
        }));
      }),
      map(({ content }) => content)
    );
  }
}
