import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  Client,
  ClientPaginationResponse,
  TablepocApiService,
} from '@animana-table-poc/table-poc/data';
import { ComponentStore } from '@ngrx/component-store';
import { finalize, map, take, tap } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'animana-table-poc-client-table',
  templateUrl: './client-table.component.html',
  styleUrls: ['./client-table.component.scss'],
  providers: [ComponentStore],
  encapsulation: ViewEncapsulation.None,
})
export class ClientTableComponent implements OnInit {
  /**
   * Selected clients from checkbox selection
   */
  public selectedClients: Client[] = [];

  /**
   * The current data loading state
   */
  public loading = false;

  /**
   * The global search value
   */
  public searchValue = '';

  public cols = [
    { field: 'firstName', header: 'First Name' },
    { field: 'lastName', header: 'Last Name' },
    { field: 'active', header: 'Active' },
  ];

  /**
   * Observable list of clients
   */
  readonly clients$ = this.componentStore.state$.pipe(
    map((state) => state.content)
  );

  constructor(
    private readonly api: TablepocApiService,
    private readonly componentStore: ComponentStore<ClientPaginationResponse>
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.api
      .getClients()
      .pipe(
        take(1),
        tap((response) => this.componentStore.setState({ ...response })),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }

  public loadClients(event: LazyLoadEvent) {
    this.loading = true;
    this.api
      .getClients(
        event.rows,
        event.first,
        'firstName',
        event.sortOrder ? 'asc' : 'desc'
      )
      .pipe(
        take(1),
        tap((response) => this.componentStore.setState({ ...response })),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }
}
