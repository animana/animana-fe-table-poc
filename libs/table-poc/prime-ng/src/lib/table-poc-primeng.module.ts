import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { ClientTableComponent } from './views/client-table/client-table.component';
import { TableModule } from 'primeng/table';

export const tablePocPrimengRoutes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: ClientTableComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(tablePocPrimengRoutes),
    TableModule,
  ],
  exports: [RouterModule],
  declarations: [ClientTableComponent],
})
export class TablePocPrimengModule {}
