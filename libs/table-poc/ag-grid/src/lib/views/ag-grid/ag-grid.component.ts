import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  Client,
  ClientPaginationResponse,
  TablepocApiService,
} from '@animana-table-poc/table-poc/data';
import { ComponentStore } from '@ngrx/component-store';
import { finalize, map, take, tap } from 'rxjs/operators';

@Component({
  selector: 'animana-table-poc-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: ['./ag-grid.component.scss'],
  providers: [ComponentStore],
  encapsulation: ViewEncapsulation.None,
})
export class AgGridComponent implements OnInit {
  columnDefs = [
    { field: 'firstName', sortable: true, filter: true },
    { field: 'lastName', sortable: true, filter: true },
    { field: 'active', sortable: true, filter: true, checkboxSelection: true },
  ];

  public selectedClients: Client[] = [];

  public loading = false;

  public searchValue = '';

  readonly clients$ = this.componentStore.state$.pipe(
    map((state) => state.content)
  );

  constructor(
    private readonly api: TablepocApiService,
    private readonly componentStore: ComponentStore<ClientPaginationResponse>
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.api
      .getClients()
      .pipe(
        take(1),
        tap((response) => this.componentStore.setState({ ...response })),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }
}
