import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { AgGridComponent } from './views/ag-grid/ag-grid.component';
import { AgGridModule } from 'ag-grid-angular';

export const tablePocAgGridRoutes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: AgGridComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(tablePocAgGridRoutes),
    AgGridModule.withComponents([]),
  ],
  exports: [RouterModule],
  declarations: [AgGridComponent],
})
export class TablePocAgGridModule {}
