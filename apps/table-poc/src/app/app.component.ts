import { Component } from '@angular/core';

@Component({
  selector: 'animana-table-poc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'table-poc';
}
