import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: 'prime-ng',
    loadChildren: () =>
      import('@animana-table-poc/table-poc-prime-ng').then(
        (m) => m.TablePocPrimengModule
      ),
  },
  {
    path: 'ag-grid',
    loadChildren: () =>
      import('@animana-table-poc/table-poc/ag-grid').then(
        (m) => m.TablePocAgGridModule
      ),
  },
  {
    path: 'ngx-datatable',
    loadChildren: () =>
      import('@animana-table-poc/table-poc/ngx-datatable').then(
        (m) => m.TablePocNgxDatatableModule
      ),
  },
  {
    path: 'html-table',
    loadChildren: () =>
      import('@animana-table-poc/table-poc/plain-table').then(
        (m) => m.TablePocPlainTableModule
      ),
  },
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent,
  },
];

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    environment.production ? [] : AkitaNgDevtools.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
