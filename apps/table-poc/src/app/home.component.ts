import { Component } from '@angular/core';

@Component({
  selector: 'animana-home-component',
  template: `
    <div>
      <ul>
        <li><a [routerLink]="['prime-ng']">PrimeNG Demo</a></li>
        <li><a [routerLink]="['ag-grid']">agGrid Demo</a></li>
        <li><a [routerLink]="['ngx-datatable']">ngx-datatable Demo</a></li>
        <li><a [routerLink]="['html-table']">HTML Table Demo</a></li>
      </ul>
    </div>
  `,
})
export class HomeComponent {}
