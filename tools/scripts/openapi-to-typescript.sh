#!/usr/bin/env bash

###
# OpenAPI to Typescript Converter
# How to Use:
#
# In the "openapi" folder, create a new folder for the name of your feature and place the "openapi.yaml" file there
# Then run "sh tools/scripts/open-api-to-typescript.sh <feature-name> <Publishable> <library-name> <openapi-file-name>
# This will create a "data" folder inside your feature folder in "libs", optionally it can be publishable which will
# provide a "package.json" and add the library as a buildable target.
# The third parameter allows the library name ("data") to be changed and the final parameter allows the openapi filename
# to be changed
###

set -o errexit -o noclobber -o nounset -o pipefail

# Global Vars
PARENT_DIR="$PWD"
ROOT_DIR="."

# Positional Parameters
FOLDER_NAME=${1:-""}
PUBLISHABLE=${2:-"False"}
LIBRARY_NAME=${3:-"data"}
OPENAPI_FILE=${4:-"openapi.yaml"}

export TS_POST_PROCESS_FILE="node_modules/.bin/prettier --write"

###
# This method generates a NX library that can be used to be a local or publishable library
# $1 Library Name
# $2 Directory Name
# $3 Publishable status
###
generateLibrary()
{
  if [[ "$3" == "--publishable" ]]; then
    echo "Generating Publishable Library for $2-$1"
    nx g @nrwl/node:library --directory="$2" --name="$1" "$3"
  else
    echo "Generating Library for $2-$1"
    nx g @nrwl/node:library --directory="$2" --name="$1"
  fi
}

###
# This method generates the the typescript folder
# $1 Library Name
# $2 Directory Name
# $3 OpenAPI File Location
###
generateTypescript()
{
  module_parts=${2/-/ }
  module_prefix=''
  while IFS= read -r -d $' ' part; do
    module_prefix="${module_prefix}$(tr '[:lower:]' '[:upper:]' <<< ${part:0:1})${part:1}"
  done <<<"$module_parts "

  openapi-generator-cli generate -i "$ROOT_DIR/openapi/$2/$3" -g typescript-angular -o "$ROOT_DIR/libs/$2/$1/src --additional-properties=apiModulePrefix=$module_prefix,serviceSuffix=ApiService --enable-post-process-file"
  # mv "$ROOT_DIR/libs/$2/$1/src/README.md" "$ROOT_DIR/libs/$2/$1"
}

###
# This method does some additional cleanup of files and folders
# $1 Library Name
# $2 Folder Name
###
preCleanupLibrary() {
  echo "Running pre-cleanup of library"
  # Clean up the existing libs folder
  rm -rf "$ROOT_DIR/libs/$2/$1/src/lib"
  rm -rf "$ROOT_DIR/libs/$2/$1/src/api"
  rm -rf "$ROOT_DIR/libs/$2/$1/src/model"
}

postCleanupLibrary() {
  echo "Running post-cleanup of library"
  # Clean up the existing libs folder
  rm "$ROOT_DIR/libs/$2/$1/src/git_push.sh"
  mv "$ROOT_DIR/libs/$2/$1/src/README.md" "$ROOT_DIR/libs/$2/$1"
}

if [[ "$FOLDER_NAME" == "" ]]; then
  echo "You must pass a library name to generate from a spec"
  exit 1
else
  if [[ ! -d "$ROOT_DIR/libs/$FOLDER_NAME/$LIBRARY_NAME" ]]; then
    generateLibrary "$LIBRARY_NAME" "$FOLDER_NAME" "$PUBLISHABLE"
  fi
  preCleanupLibrary "$LIBRARY_NAME" "$FOLDER_NAME"
  generateTypescript "$LIBRARY_NAME" "$FOLDER_NAME" "$OPENAPI_FILE"
  postCleanupLibrary "$LIBRARY_NAME" "$FOLDER_NAME"
fi
