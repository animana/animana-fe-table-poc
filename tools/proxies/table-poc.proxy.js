const PROXY_CONFIG = {
  '/poc': {
    target: 'http://localhost:9080',
    secure: false,
    changeOrigin: true,
    logLevel: 'debug',
  },
};

module.exports = PROXY_CONFIG;
