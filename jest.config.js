module.exports = {
  projects: [
    '<rootDir>/apps/table-poc',
    '<rootDir>/libs/table-poc/prime-ng',
    '<rootDir>/libs/table-poc/data',
    '<rootDir>/libs/table-poc/shared',
    '<rootDir>/libs/table-poc/ag-grid',
    '<rootDir>/libs/table-poc/ngx-datatable',
    '<rootDir>/libs/table-poc/plain-table',
  ],
};
